#!/bin/bash

set -e

docker run -d -p 80:80 \
	--name vhost-proxy \
	-v /var/run/docker.sock:/tmp/docker.sock:ro \
	-v ${PWD}/files/nginx-proxy-config.conf:/etc/nginx/conf.d/nginx-proxy-config.conf:ro \
	jwilder/nginx-proxy
